#include <unistd.h>

#include <cstdint>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

#include "asio.hpp"

int main() {
    asio::io_service ioc;
    asio::ip::tcp::endpoint ep(asio::ip::tcp::v4(), 9527);
    asio::ip::tcp::acceptor acc(ioc, ep);

    while (true) {
        auto socket = std::make_shared<asio::ip::tcp::socket>(ioc);
        std::cout << "waiting acc" << std::endl;
        acc.accept(*socket);
        uint8_t data[1024];
        while (true) {
            size_t len;
            try {
                usleep(400 * 1000);
                len = socket->read_some(asio::buffer(data));
            } catch (std::system_error e) {
                std::cout << e.what() << std::endl;
                break;
            }

            std::string s(data, data + len);
            std::cout << "len = " << s.size() << std::endl;
            std::cout << s << std::endl;
        }
    }

    asio::ip::tcp::socket socket(ioc);
    socket.connect(ep);
    std::cout << "hello, asio" << std::endl;
    return 0;
}

