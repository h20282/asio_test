#include <unistd.h>

#include <iostream>

#include "asio.hpp"

int main() {
    asio::io_service ioc;
    asio::ip::tcp::endpoint eq(asio::ip::address::from_string("127.0.0.1"),
                               9527);
    asio::ip::tcp::socket socket(ioc);
    socket.connect(eq);
    char msg[] = "hello, asio";
    while (true) {
        socket.send(asio::buffer(msg));
        usleep(100 * 1000);
    }
    return 0;
}

